# AMSAT LIST DATABASE (Français)

"Le projet "AMATEUR SATELLITE DATABASE " consiste à définir une structure de données afin d'aider les utilisateurs de la radioamateur par satellite à partager des informations sur les satellites amateurs.

La description des satellites amateurs est intégrée dans un fichier XML.  le schéma du fichier XML AMATEUR SATELLITE XML défini la manière d'organiser les données. Il pourrait être utilisé pour valider une description de Satellites Amateurs.

Ce projet maintient également une description de référence des satellites amateurs (format XML). Il fournit également un fichier XLT afin de construire un fichier HTML sous forme d'exemple.
This project is open source and supported by Amsat-Francophone

# AMSAT LIST DATABASE (Anglais)

"AMATEUR SATELLITE DATABASE" project is to define a data structure to help amateur satellite radio users to share information about amateur satellites.

The amateur satellite description is integrated in an XML file. The AMATEUR SATELLITE XML file schema defines how to organize the data. It could be used to validate a description of Amateur Satellites.

This project also maintains a reference description of amateur satellites (XML format). It also provides an XLT file to build an HTML file as an example.
This project is open source and supported by Amsat-Francophone


